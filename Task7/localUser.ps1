﻿param(
    [Parameter(Mandatory)][string]$userName,
    [Parameter(Mandatory)][string]$password,
    [Parameter(Mandatory)][string]$folderPath,
    [Parameter(Mandatory)][string]$folderName,
    [Parameter(Mandatory)][string]$serviceName
)
#Check if PS Module Carbon is installed. If not then install.
If(!(Get-Module -Name Carbon -ListAvailable)){
    Find-Module -Name Carbon | Install-Module -Force
    Write-Host "Carbon PSModule will be installed" -ForegroundColor DarkCyan}
else{Import-Module Carbon
    Write-Host "Carbon PSModule is already installed" -ForegroundColor DarkCyan}

#Create new local username with password
$user = New-LocalUser -Name $userName -Password ($password | ConvertTo-SecureString -AsPlainText -Force) -AccountNeverExpires -PasswordNeverExpires

#Give Logon As Service permission for this user
Grant-CPrivilege -Identity $user.name -Privilege "SeServiceLogonRight"

#Create new folder and give Full Control access to the username
$folder = New-Item -ItemType Directory -Path $folderPath -Name $folderName
$acl = Get-Acl $folder
$newAcl = New-Object System.Security.AccessControl.FileSystemAccessRule("$user", "FullControl", "Allow")
$acl.SetAccessRule($newAcl)
Set-Acl -Path $folder -AclObject $acl

#Give Service Control access to the username
Grant-ServiceControlPermission -ServiceName $serviceName -Identity $user.Name