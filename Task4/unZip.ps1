﻿param(
    [Parameter(Mandatory, HelpMessage="Enter uri where the 7z.exe will be downloaded from")]
    [string]$uri,
    [Parameter(Mandatory, HelpMessage="Enter a path to save the 7z.exe file")]
    [string]$savePath,
    [Parameter(Mandatory, HelpMessage="Enter archive file path")]
    [string]$archiveFile,
    [Parameter(Mandatory, HelpMessage="Enter a path to extract files from archive")]
    [string]$unZipDestination,
    [switch]$x86bit
)
#Download 7z.exe file
Invoke-WebRequest -Uri $uri -OutFile $savePath

#Install 7z.exe silently  
Start-Process -FilePath $savePath -ArgumentList '/S' -Wait

#Unzip archive
if($x86bit){
    Start-Process 'C:\Program Files (x86)\7-Zip\7z.exe' -ArgumentList "e $archiveFile -o$unZipDestination" -Wait
}
else{
    Start-Process 'C:\Program Files\7-Zip\7z.exe' -ArgumentList "e $archiveFile -o$unZipDestination" -Wait
}
