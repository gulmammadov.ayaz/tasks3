﻿param(
    [string]$displayName,
    [int]$portNumber,
    [ValidateSet('Inbound', 'Outbound')][string]$direction,
    [ValidateSet('TCP', 'UDP')][string]$protocol,
    [ValidateSet('Any', 'Domain', 'Public', 'Private')][string]$profile
)
New-NetFirewallRule -DisplayName $displayName -Direction $direction -LocalPort $portNumber -Profile $profile -Action Allow -Enabled True