﻿param(
    [securestring]$password = (Read-Host -Prompt "Type password of the certificate" | ConvertTo-SecureString -AsPlainText -Force )
)
$cert = Import-PfxCertificate -FilePath D:\AzureTasks\Task6\Cert\azureiis.pfx -Password $password -CertStoreLocation Cert:\LocalMachine\My
$cert.Thumbprint