﻿function changeString{
    param(
        [Parameter(Mandatory = $true, Position=0)]
        [string]$filePath,
        [Parameter(Mandatory = $true, Position=1)]
        [string]$firstValue,
        [Parameter(Mandatory = $true, Position=2)]
        [string]$secondValue,
        [Parameter(Mandatory = $true)]
        [string]$replaceString
    )
    (Get-Content $file) -replace $replaceString, "$firstValue $secondValue" 
}