﻿param(
    [string]$RGName,
    [string]$vmName,
    [string]$strAccountName,
    [string]$localPath,
    [string]$logFileName,
    [array]$filter

)
$vm = Get-AzureRmVM -ResourceGroupName $RGName -Name $vmName
Set-AzureRmVMBootDiagnostics -ResourceGroupName $RGName -VM $vm -StorageAccountName $strAccountName -Enable
Get-AzureRmVMBootDiagnosticsData -ResourceGroupName $RGName -Name $vm.Name -Windows -LocalPath $localPath
Get-Content $localPath\$logFileName | Select-String -Pattern $filter -SimpleMatch