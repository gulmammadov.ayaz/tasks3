function changeString{
    param(
        [string]$string,
        [string]$newString,
        [string]$filePath
    )
$sample =  "Hello Ayaz!"
if(!(Get-Content $filePath | Select-String $string)){
    Write-Host "The string you have specified does not exist"}
else{
$newContent = (Get-Content -Path $filePath).Replace("$string","$newString")
Set-Content -Value $newContent -Path $filePath
Write-Host "The string '$string' has been changed to '$newString' inside the function you have choosed"}
}
